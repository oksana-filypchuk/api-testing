import { ApiRequest } from '../request';

const baseUrl: string = 'http://tasque.lol/';

export class RegisterController { 
    async registerUser(emailValue: string, userNameValue: string, passwordValue: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Register`)
            .body({
                avatar: "https://www.myAvatar.com/avatar.jpg",
                email: emailValue,
                userName: userNameValue,
                password: passwordValue
            })
            .send();
        return response;
    }
}