import { ApiRequest } from '../request';

const baseUrl: string = 'http://tasque.lol/';

export class PostController {
    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts`)
            .send();
        return response;
    }
    async createPost(previewImage: string, textbody: string, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body({
                previewImage: previewImage,
                body: textbody
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }
    async likePost(entityId: number, isLike: boolean, userId: number, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body({
                entityId: entityId,
                isLike: isLike,
                userId: userId
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }
}