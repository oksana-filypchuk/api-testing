import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";


const users = new UsersController();
const schemasAllUsers = require('./data/schemas_allUsers.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe("UserController", () => {

    it(`should return 200 status code and all users when getting the user collection`, async () => {
        let response = await users.getAllUsers();

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
        expect(response.body).to.be.jsonSchema(schemasAllUsers.schema_allUsers); 
    });
});