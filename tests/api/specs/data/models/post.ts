export interface Post {
    id: number,
    previewImage: string,
    body: string
}