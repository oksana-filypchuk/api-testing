export interface User {
    id: number,
    avatar: string,
    email: string,
    userName: string
}