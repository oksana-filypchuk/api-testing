import { Post } from './data/models/post';
import { expect } from "chai";
import { PostController } from "../lib/controllers/posts.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";


const auth = new AuthController();
const posts = new PostController();
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe("PostController", () => {
    let accessToken: string;
    let userId: number;
    let lastPost: Post;

    before(`Login and get the token`, async () => {
        
        let response = await auth.login("mytestUs@gmail.com", "12345678");

        accessToken = response.body.token.accessToken.token;
        userId = response.body.user.id;

    });

    it(`we should get last created post`, async () => {
        let response = await posts.getAllPosts();
        lastPost = response.body[0];
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(lastPost.id).to.be.greaterThan(0);
    });


    it(`should return 200 status code and like post`, async () => {
        let response = await posts.likePost(lastPost.id, true, userId, accessToken);
        

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
    });

    it(`we should get liked post`, async () => {
        let response = await posts.getAllPosts();
        lastPost = response.body[0];
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body[0].reactions.length).to.be.greaterThan(0);
    });
});