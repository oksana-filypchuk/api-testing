import { expect } from "chai";
import { RegisterController } from "../lib/controllers/register.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";


const register = new RegisterController();
const schemasRegister = require('./data/schemas_register.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe("RegisterController", () => {

    it(`should return 201 status code and create new user`, async () => {
        let response = await register.registerUser('myemail1234@gmail.com', 'myUserTest', '12345678');
        
        checkStatusCode(response, 201);
        checkResponseTime(response,2000);
        expect(response.body).to.be.jsonSchema(schemasRegister.schema_registerUser);
    });

      //Negative test scenario
    it(`New user should be not registered`, async () => {
        let response = await register.registerUser('myemail1234.com', 'myUserTest', '12345678');

        expect(response.statusCode).to.be.not.equal(201);
    });
});