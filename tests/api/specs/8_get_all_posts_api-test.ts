import { expect } from "chai";
import { PostController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";


const posts = new PostController();
const schemas = require('./data/schemas_allPosts.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe("PostController", () => {

    it(`should return 200 status code and all posts when getting the post collection`, async () => {
        let response = await posts.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
        expect(response.body).to.be.jsonSchema(schemas.schema_allPosts);
    });
});