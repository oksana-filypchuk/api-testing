import { User } from './data/models/user';
import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const auth = new AuthController();
const users = new UsersController();
const schemas = require('./data/schemas_user.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe("UserController", () => {
    let accessToken: string;
    let userId: number;

    before(`Login and get the token`, async () => {
        let response = await auth.login("myemail1234@gmail.com", "12345678");

        accessToken = response.body.token.accessToken.token;
        userId = response.body.user.id;

    });
    it(`should return 200 status code and user by token should be got`, async () => {
        let response = await users.getUserFromToken(accessToken);
       
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body.id, `Response id should have be equal user data`).to.be.equal(userId); 
        expect(response.body).to.be.jsonSchema(schemas.schema_userFromToken);
    });
});