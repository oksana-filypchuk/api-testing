import { expect } from "chai";
import { PostController } from "../lib/controllers/posts.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";


const auth = new AuthController();
const posts = new PostController();
const schemas = require('./data/schemas_post.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe("PostController", () => {
    let accessToken: string;
    let userId: number;

    before(`Login and get the token`, async () => {
        
        let response = await auth.login("mytestUs@gmail.com", "12345678");

        accessToken = response.body.token.accessToken.token;
        userId = response.body.user.id;

    });

    it(`should return 200 status code and create new post`, async () => {
        let response = await posts.createPost('https://www.myPost.com/post.jpg', 'Hello, here!', accessToken);
        
        checkStatusCode(response, 200);
        checkResponseTime(response,2000);
        expect(response.body).to.be.jsonSchema(schemas.schema_createPost);
    });
});