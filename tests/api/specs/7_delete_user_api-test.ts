import { User } from './data/models/user';
import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const auth = new AuthController();
const users = new UsersController();

describe("UserController", () => {
    let accessToken: string;
    let userId: number;

    before(`Login and get the token`, async () => {
        let response = await auth.login("myemail1234@gmail.com", "12345678");

        accessToken = response.body.token.accessToken.token;
        userId = response.body.user.id;

    });

    it(`should return 204 status code and user is deleted`, async () => {
        let response = await users.deleteUser(userId, accessToken);
       
        checkStatusCode(response, 204);
        checkResponseTime(response,1000);
    });

    it(`should return 404 status code and user is not found`, async () => {
        let response = await users.getUserById(userId);
       
        checkStatusCode(response, 404);
        checkResponseTime(response,1000);
    });

    //Negative test scenario
    it(`should return status code that not equal 204`, async () => {
        let accessToken = "notValideToken";
        let response = await users.deleteUser(userId, accessToken);
       
        expect(response.statusCode).to.be.not.equal(204);
    });
});