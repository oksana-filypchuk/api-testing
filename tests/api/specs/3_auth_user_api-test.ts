import { User } from './data/models/user';
import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const auth = new AuthController();
const schemasRegister = require('./data/schemas_register.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe("Token usage", () => {

    it(`Login user`, async () => {
        let userData: User = {
            id: 0,
            avatar: "https://www.myAvatar.com/avatar.jpg",
            email: "myemail1234@gmail.com",
            userName: "myUserTest",
        };

        let response = await auth.login("myemail1234@gmail.com", "12345678");

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body.token.accessToken.token.length, `Response token should have more than 50 item`).to.be.greaterThan(50);
        expect(response.body.user.userName, 'Responced user name should be equal user data').to.be.equal(userData.userName);
        expect(response.body.user.email, 'Responced email should be equal to user data').to.be.equal(userData.email);
        expect(response.body).to.be.jsonSchema(schemasRegister.schema_registerUser);
    });

    //Negative test scenario
    it(`Login user with invalid email and user should be not logged`, async () => {
        
        let response = await auth.login("myemail1234gmail.com", "12345678");

        expect(response.statusCode).to.be.not.equal(200);
    });
});
